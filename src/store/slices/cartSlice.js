import { createSlice } from "@reduxjs/toolkit";

const items = localStorage.getItem('cart') !== null ? JSON.parse(localStorage.getItem('cart')) : []
const totalPrice = localStorage.getItem('totalPrice') !== null ? JSON.parse(localStorage.getItem('totalPrice')) : 0
const totalItems = localStorage.getItem('totalItems') !== null ? JSON.parse(localStorage.getItem('totalItems')) : 0
const initialState = {
    cart: items,
    totalPrice: totalPrice,
    totalItems: totalItems,
    totalPriceAndShip: 0,
};

const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        addItem: (state, action) => {
            const obj = state.cart.find((item) => item.id === action.payload.id);
            if (obj) {
                obj.qty++;
            } else {
                state.cart.push({ ...action.payload,"product_id": action.payload?.id , qty: 1 });
            }
            localStorage.setItem('cart', JSON.stringify(state.cart.map(item=>item)))
            localStorage.setItem('totalPrice', JSON.stringify(state.totalPrice))
            localStorage.setItem('totalItems', JSON.stringify(state.totalItems))
        },
        removeItem: (state, action) => {
            state.cart = state.cart.filter((item) => item.id !== action.payload);
            localStorage.setItem('cart', JSON.stringify(state.cart.map(item=>item)))
            localStorage.setItem('totalPrice', JSON.stringify(state.totalPrice))
            localStorage.setItem('totalItems', JSON.stringify(state.totalItems))
        },
        removeAllItems: (state, action) => {
            state.cart = [];
            localStorage.setItem('cart', JSON.stringify(state.cart?.map(item=>item)))
            localStorage.setItem('totalPrice', JSON.stringify(state.totalPrice))
            localStorage.setItem('totalItems', JSON.stringify(state.totalItems))
        },
        increaseQuantity: (state, action) => {
            const obj = state.cart.find((item) => item.id === action.payload);
            obj.qty++;
            localStorage.setItem('cart', JSON.stringify(state.cart.map(item=>item)))
            localStorage.setItem('totalPrice', JSON.stringify(state.totalPrice))
            localStorage.setItem('totalItems', JSON.stringify(state.totalItems))
        },
        decreaseQuantity: (state, action) => {
            const obj = state.cart.find((item) => item.id === action.payload);
            if (obj.qty > 1) obj.qty--;
            localStorage.setItem('cart', JSON.stringify(state.cart.map(item=>item)))
            localStorage.setItem('totalPrice', JSON.stringify(state.totalPrice))
            localStorage.setItem('totalItems', JSON.stringify(state.totalItems))
        },
        calcTotal: (state) => {
            state.totalPrice = state.cart?.reduce(
                (acc, item) => acc + item.qty * (item.price - item.offer), 0
            );
            state.totalItems = state.cart?.reduce(
                (acc, item) => acc + item.qty, 0
            );
            localStorage.setItem('totalPrice', JSON.stringify(state.totalPrice))
            localStorage.setItem('totalItems', JSON.stringify(state.totalItems))
        },
        
    },
});
export const {
    addItem,
    removeItem,
    removeAll,
    increaseQuantity,
    decreaseQuantity,
    calcTotal,
    removeAllItems
} = cartSlice.actions;
export const cartReducer = cartSlice.reducer;
